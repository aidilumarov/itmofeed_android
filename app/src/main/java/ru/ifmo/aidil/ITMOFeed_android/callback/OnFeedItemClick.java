package ru.ifmo.aidil.ITMOFeed_android.callback;

import ru.ifmo.aidil.ITMOFeed_android.data.DataModel;


public interface OnFeedItemClick {

    void passData(DataModel d);

}
