package ru.ifmo.aidil.ITMOFeed_android.API;

import java.util.ArrayList;

import ru.ifmo.aidil.ITMOFeed_android.data.DataModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface MyBackendAPI {

    @GET("?format=json")
    Call<ArrayList<DataModel>> loadData();

}
